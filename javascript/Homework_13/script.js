const changeBtn = document.getElementById('change-btn');

const table = document.getElementById('table');

const standard = document.getElementById('standard');
const ultimate = document.getElementById('ultimate');
const premium = document.getElementById('premium');

const contactBtn = document.getElementById('button-contact');
const standartBtn = document.getElementById('button-buy-standard');
const ultimateBtn = document.getElementById('button-buy-ultimate');
const premiumBtn = document.getElementById('button-buy-premium');

if (localStorage.getItem('border')) {
    table.classList.add('table');
}
if (localStorage.getItem('standard')) {
    standard.classList.add('head-standard');
}
if (localStorage.getItem('ultimate')) {
    ultimate.classList.add('head-ultimate');
}
if (localStorage.getItem('premium')) {
    premium.classList.add('head-premium');
}
if (localStorage.getItem('contactBtn')) {
    contactBtn.classList.add('button-contact');
}
if (localStorage.getItem('standartBtn')) {
    standartBtn.classList.add('button-buy-standard');
}
if (localStorage.getItem('ultimateBtn')) {
    ultimateBtn.classList.add('button-buy-ultimate');
}
if (localStorage.getItem('premiumBtn')) {
    premiumBtn.classList.add('button-buy-premium');
}

changeBtn.addEventListener('click', () => {
    if (table.classList.contains('table-dark')) {
        table.classList.remove('table-dark');
        table.classList.add('table');
        localStorage.setItem('border', 'true');
    } else {
        table.classList.add('table-dark');
        table.classList.remove('table');
        localStorage.clear('border');
    }
    if (standard.classList.contains('head-standard-dark')) {
        standard.classList.remove('head-standard-dark');
        standard.classList.add('head-standard');
        localStorage.setItem('standard', 'true');

    } else {
        standard.classList.add('head-standard-dark');
        standard.classList.remove('head-standard');
        localStorage.clear('standard');
    }
    if (ultimate.classList.contains('head-ultimate-dark')) {
        ultimate.classList.remove('head-ultimate-dark');
        ultimate.classList.add('head-ultimate');
        localStorage.setItem('ultimate', 'true');

    } else {
        ultimate.classList.add('head-ultimate-dark');
        ultimate.classList.remove('head-ultimate');
        localStorage.clear('ultimate');
    }
    if (premium.classList.contains('head-premium-dark')){
        premium.classList.remove('head-premium-dark');
        premium.classList.add('head-premium');
        localStorage.setItem('premium', 'true');

    } else {
        premium.classList.add('head-premium-dark');
        premium.classList.remove('head-premium');
        localStorage.clear('premium');
    }
    if (contactBtn.classList.contains('button-contact-dark')){
        contactBtn.classList.remove('button-contact-dark');
        contactBtn.classList.add('button-contact');
        localStorage.setItem('contactBtn', 'true');

    } else {
        contactBtn.classList.add('button-contact-dark');
        contactBtn.classList.remove('button-contact');
        localStorage.clear('contactBtn');

    }
    if (standartBtn.classList.contains('button-buy-standard-dark')){
        standartBtn.classList.remove('button-buy-standard-dark');
        standartBtn.classList.add('button-buy-standard');
        localStorage.setItem('standartBtn', 'true');
    } else {
        standartBtn.classList.add('button-buy-standard-dark');
        standartBtn.classList.remove('button-buy-standard');
        localStorage.clear('standartBtn');
    }
    if (ultimateBtn.classList.contains('button-buy-ultimate-dark')){
        ultimateBtn.classList.remove('button-buy-ultimate-dark');
        ultimateBtn.classList.add('button-buy-ultimate');
        localStorage.setItem('ultimateBtn', 'true');
    } else {
        ultimateBtn.classList.add('button-buy-ultimate-dark');
        ultimateBtn.classList.remove('button-buy-ultimate');
        localStorage.clear('ultimateBtn');
    }
    if (premiumBtn.classList.contains('button-buy-premium-dark')){
        premiumBtn.classList.remove('button-buy-premium-dark');
        premiumBtn.classList.add('button-buy-premium');
        localStorage.setItem('premiumBtn', 'true');
    } else {
        premiumBtn.classList.add('button-buy-premium-dark');
        premiumBtn.classList.remove('button-buy-premium');
        localStorage.clear('premiumBtn');
    }
});







