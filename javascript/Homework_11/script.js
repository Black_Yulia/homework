const enter = document.getElementById('enter');
document.addEventListener ('keydown', () => {
    if (event.key === 'Enter') {
        enter.classList.add('active-btn')
    } else {
        enter.classList.remove('active-btn');
    }
});

const letterS = document.getElementById('letter-S');
document.addEventListener ('keydown', () => {
    if (event.key === 'S') {
        letterS.classList.add('active-btn')
    } else {
        letterS.classList.remove('active-btn')
    }
});

const letterE = document.getElementById('letter-E');
document.addEventListener ('keydown', () => {
    if (event.key === 'E') {
        letterE.classList.add('active-btn')
    } else {
        letterE.classList.remove('active-btn')
    }
});

const letterO = document.getElementById('letter-O');
document.addEventListener ('keydown', () => {
    if (event.key === 'O') {
        letterO.classList.add('active-btn')
    } else {
        letterO.classList.remove('active-btn')
    }
});

const letterN = document.getElementById('letter-N');
document.addEventListener ('keydown', () => {
    if (event.key === 'N') {
        letterN.classList.add('active-btn')
    } else {
        letterN.classList.remove('active-btn')
    }
});

const letterL = document.getElementById('letter-L');
document.addEventListener ('keydown', () => {
    if (event.key === 'L') {
        letterL.classList.add('active-btn')
    } else {
        letterL.classList.remove('active-btn')
    }
});

const letterZ = document.getElementById('letter-Z');
document.addEventListener ('keydown', () => {
    if (event.key === 'Z') {
        letterZ.classList.add('active-btn')
    } else {
        letterZ.classList.remove('active-btn')
    }
});