const $tabsTitle = $('.tabs-title');

$tabsTitle.click(function() {
    const index = $(this).attr('data-tab');
    const content = $('.tabs-content-item[data-tab="'+ index +'"]');

    $('.tabs-title.active').removeClass('active');
    $(this).addClass('active');

    $('.tabs-content-item.tabs-content-item-active').removeClass('tabs-content-item-active');
    content.addClass('tabs-content-item-active');
});