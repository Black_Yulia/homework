const image = document.getElementById('first-img');
const imgArray = ['./img/1.jpg','./img/2.jpg', './img/3.JPG', './img/4.png'];
let index = 1;
function showImg() {
    image.setAttribute("src", imgArray[index]);
    index++;
    if(index >= imgArray.length)
    {
        index=0;
    }
}
let slide = setInterval(showImg,10000);

const activeBtn = document.getElementById('btn-active');
const stopBtn = document.getElementById('btn-stop');
stopBtn.addEventListener('click', () => {
   clearInterval(slide);
   activeBtn.style.display = 'inline-block'
});

activeBtn.addEventListener('click' , ()=> {
    slide = setInterval(showImg,10000);
});




// Для таймера
// let timer = setInterval(countTimer, 1000);
// let totalSeconds = 10;
// function countTimer() {
//     --totalSeconds;
//     let hour = Math.floor(totalSeconds /3600);
//     let minute = Math.floor((totalSeconds - hour*3600)/60);
//     let seconds = totalSeconds - (hour*3600 + minute*60);
//     document.getElementById("timer").innerHTML = hour + ":" + minute + ":" + seconds;
// }
