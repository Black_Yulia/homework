**1.Опишите своими словами разницу между функциями setTimeout() и setInterval().**

За допомогою даних методів можна запланувати виклик функції не в даний момент, а через певний проміжок часу. Тобто, відстрочити виконання функції.
Головна відмінність даних методів в тому, що за допомогою `setTimeout()` ми можемо викликати функцію **один раз** через певний проміжок часу. А `setInterval()` дозволяє нам викликати функцію **регулярно** через певний проміжок часу.

**2.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?**

Миттєво дана функція не спрацює. Якщо функції присвоєний метод `setTimeout()`, то вона видаляється із загальної черги виконання і планувальник викличе її тільки після завершення виконання поточного коду.
Тобто, спочатку виконується увесь поточний код, а всі функції з `setTimeout()` поміщаються в окрему чергу. І відразу після закінчення виконання поточного коду, відпрацюють функції з нульовою затримкою в `setTimeout()`.

Наприклад, даний код:

console.log(1);

setTimeout(function() {console.log(2)});

console.log(3);

console.log(4);

console.log(5);

В консолі буде виведений в такому порядку: 1,3,4,5,2

**3.Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?**

Тому що `setInterval()` викликає функцію регулярно через певний проміжок часу. Регулярно означає безкінечну кількість раз, доки ми не зупинимо цей цикл. Саму тому необхідно викликати `clearInterval()`, який дозволяє нам припинити виконання `setInterval()` в той момент,коли нам необхідно.