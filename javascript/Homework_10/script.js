const firstPassword = document.getElementById('first-password');
const firstIcon = document.getElementById('first-icon');

firstIcon.addEventListener('click', () => {
   if (firstPassword.type === "password") {
      firstPassword.type = "text";                      // firstPassword.setAttribute('type', 'text');   // Так теж працює
      firstIcon.classList.remove('icon-password-hide');
      firstIcon.classList.add('icon-password-show');
   } else {
      firstPassword.type = "password";
      firstIcon.classList.remove('icon-password-show');
      firstIcon.classList.add('icon-password-hide');
   }
});



const secondPassword = document.getElementById('second-password');
const secondIcon = document.getElementById('second-icon');

secondIcon.addEventListener('click', () => {
   if (secondPassword.type === "password") {
      secondPassword.type = "text";
      secondIcon.classList.remove('icon-password-hide');
      secondIcon.classList.add('icon-password-show');
   } else {
      secondPassword.type = "password";
      secondIcon.classList.remove('icon-password-show');
      secondIcon.classList.add('icon-password-hide');
   }
});


const uncorrectValue = document.getElementById('uncorrect-value');
const btnSubmit = document.getElementById('btn');

btnSubmit.addEventListener('click', () => {
   if (firstPassword.value==='') {
      firstPassword.classList.add('enter-password');
   } else if (firstPassword.value===secondPassword.value){
      firstPassword.classList.remove('enter-password');
      uncorrectValue.style.display = 'none';
      alert('You are welcome');
   } else {
      uncorrectValue.style.display = 'block';
      firstPassword.classList.remove('enter-password');
   }
});


