const tabsTitle = document.querySelectorAll('.tabs-title');
const content = document.querySelectorAll('.tabs-content li');

for(let i=0; i<tabsTitle.length; i++) {
    (function(item) {
        let link = tabsTitle[item];
        link.addEventListener('click', () => {
            for(let j=0; j<content.length; j++) {
                let showText = window.getComputedStyle(content[j]).display;
                if(showText === "block") {
                    content[j].style.display = "none";
                    tabsTitle[j].classList.remove('active');
                }
            }
            content[item].style.display = "block";
            link.classList.add('active');
        })
    })(i);
}