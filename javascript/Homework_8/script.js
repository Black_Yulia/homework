const inputBlock = document.getElementById('inputBlock');
inputBlock.type = 'number';

inputBlock.addEventListener('focus', () => {
    inputBlock.style.border = '3px solid green'
});

inputBlock.addEventListener('blur', function()  {
    inputBlock.style.border = '3px solid mediumvioletred';
    inputBlock.style.color = 'green';
    const value = this.value;
    if (value < 0){
        inputBlock.style.border = '3px solid red';
        modalwin.style.border ='none';
        spanText.style.display ='none';
        const uncorrectPrice = document.createElement('p');
        inputBlock.after(uncorrectPrice);
        uncorrectPrice.innerText = 'Please enter correct price';
        uncorrectPrice.classList.add('uncorrectPrice');
    }
});


const spanText = document.getElementById('spanText');
inputBlock.addEventListener('blur', () => {
    spanText.innerText = `Текущая цена: ${inputBlock.value}`;
 });

const modalwin = document.getElementById('modalwin');
inputBlock.addEventListener('blur', () => {
    modalwin.style.display = 'inline-block';
});

spanText.addEventListener('click', () => {
    modalwin.style.display = 'none';
    inputBlock.value = ''
});