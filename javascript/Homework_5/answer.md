**1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.**

Екранування - це коли ми уникаємо спеціальних символів в коді, коли не хочемо, щоб вони мали своє особливе значення.

Наприклад, у таких символів >, <, &, '' є своє спеціальне значення і їх використання сигналізує про якусь операцію/дію. Але часто нам потрібно використовувати такі символи у звичайному тексті. Тому, щоб вони не сприймалися, як особливі символи в коді, їх екранують, тобто замінюють на певну послідновність звичайних символів. При відображенні, на сторінці, вони будуть виглядати, як звичайні зрозумілі нам символи, але в коді вони будуть 'створені' за допомогою набору різних символів.
Наприклад, символ > при екрануванні в коді буде прописуватися як \u003E, але при відображенні на сторінці це буде звичайний символ >.

Екранування потрібне для того, щоб, наприклад, браузер, правильно "розумів", де звичайний текст, в якому присутні спеціальні символи, а де код, який оперує цими символами.


