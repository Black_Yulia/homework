const createNewUser = (firstName, lastName) => {
    const newUser = {
        firstName,
        lastName,
        getLogin () {
        return (this.firstName[0] + this.lastName).toLowerCase()
        },
        setFirstName(name){
            Object.defineProperty(newUser, 'firstName', {
                value: name
            })
        },
        setLastName(lastName){
            Object.defineProperty(newUser, 'lastName', {
                value: lastName
            })
        }
    };
    Object.defineProperty(newUser, 'firstName', {
        writable:false,
        value: newUser.firstName
    });
    Object.defineProperty(newUser, 'lastName', {
        writable:false,
        value: newUser.lastName
    });
    return newUser;
};

const newUser = createNewUser(prompt('Enter your name'),
prompt('Enter your last name'));
console.log(newUser);
console.log(newUser.getLogin());
