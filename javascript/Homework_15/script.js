/*Для якорів*/

$(document).ready(function(){
    $("#menu").on("click","a", function (event) {
        event.preventDefault();
        const $elementClick  = $(this).attr('href');
        const $destination = $($elementClick).offset().top;
        $('body,html').animate({
            scrollTop: $destination
        }, 1000);
    });
});


$('.button-hide').on('click', () => {
    $('.hot-news-wrapper').slideToggle();
});

/*кнопка "Наверх"*/

$(document).scroll(function () {
    const $screenHeight = $(window).innerHeight();
    const $screenTop = $(window).scrollTop();
    if ($screenTop > $screenHeight) {
        if (!$('.scroll-top-btn').length) {
            const $scrolTopBtn = $('<button hidden class="scroll-top-btn">&#8679;</button>');
            $('script:first').before($scrolTopBtn);
            $scrolTopBtn.fadeIn();
            $scrolTopBtn.click(() => {
                $('body, html').animate({
                    scrollTop: 0
                }, 1000);
            })
        }
    } else {
        $('.scroll-top-btn').fadeOut( () => {
            $('.scroll-top-btn').remove();
        })
    }
});






