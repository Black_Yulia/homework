const array = ['1', '2', '3', 'sea', 'user', 23];

function createList (array) {

    const list = document.createElement('ul');

    const arrayListItems = array.map(item=>{
        const listItem = document.createElement('li');
        listItem.innerText = item;
        return listItem;

    });
    console.log(arrayListItems);
    arrayListItems.forEach(li => {
        list.appendChild(li)
    });

    return list;
}

document.querySelector('script').before(createList(array));