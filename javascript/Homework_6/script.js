function filterBy (array, value) {
  return  array.filter(item => typeof item !== value)
}

const array = ['hello', 'world', 23, '23', 30, 40];
const filtered = filterBy(array, 'string');
console.log(filtered);


// function filterBy (value) {
//     return typeof value === 'string';
// }
// const filtered = ['hello', 'world', 23, '23', 30, null].filter(filterBy);
//
// console.log(filtered);


