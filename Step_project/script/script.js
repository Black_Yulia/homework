// Переключение вкладок в секции Our services

const $tabsTitle = $('.services-title');

$tabsTitle.click(function() {
    const index = $(this).attr('data-tab');
    const content = $('.services-content[data-tab="'+ index +'"]');

    $('.services-title.services-title-active').removeClass('services-title-active');
    $(this).addClass('services-title-active');

    $('.services-content.services-content-active').removeClass('services-content-active');
    content.addClass('services-content-active');
});


// Кнопка Load more в секции Our amazing work

const $loadMore = $('.load-more-btn');

$loadMore.click( function () {
    const $secondShow = $('.works-img-show-second');
    $secondShow.fadeIn(1000, () => {
        $secondShow.css({display: 'inline-block'});
    });

    $loadMore.css({display: 'none'});
});


// Кнопки на вкладке Our amazing work для "фильтрации продукции"

const $worksTitle = $('.works-title');

$worksTitle.click(function() {
    const index = $(this).attr('data-category');
    const content = $('.works-img-block[data-category="'+ index +'"]');

    $('.works-title.works-title-active').removeClass('works-title-active');
    $(this).addClass('works-title-active');

    $('.works-img-block').css({display: 'none'});
    content.css({display:'inline-block'});

    $loadMore.css({display: 'none'});
});

const $allImgs = $('#all-img');

$allImgs.click(function() {
    $('.works-img-show').css({display: 'inline-block'});
    $loadMore.css({display: 'block'});
});


// Карусель на вкладке What people say about theHam

$('.people-say-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
});

const $sliderImg = $('.people-say-slider-img');

$sliderImg.click(function() {
    const index = $(this).attr('data-quote');
    const content = $('.people-say-quote[data-quote="'+ index +'"]');

    $('.people-say-slider-img.people-say-slider-img-active').removeClass('people-say-slider-img-active');
    $(this).addClass('people-say-slider-img-active');

    $('.people-say-quote.people-say-quote-active').removeClass('people-say-quote-active');
    content.addClass('people-say-quote-active');
});